import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {Redirect} from 'react-router'

class Home extends Component {

    render(){

        var redirectVar
        if (localStorage.getItem('isOwner') === "false") {
            redirectVar = <Redirect to="/user/home" />
        } else if (localStorage.getItem('isOwner') === "true") {
            redirectVar = <Redirect to="/owner/home" />
        }

        return(
            <div class="row" style={{ minHeight: 100 + "vh"}}>
                { redirectVar }
                <div class="col-sm-6 bg-info pt-5 pl-5 shadow text-center">
                    <p class="display-2">GRUBHUB</p>
                </div>
                <div class="col-sm-6">
                    <h4 class="text-right pr-5 pt-5"><Link to="/login">Sign in</Link></h4>
                    <h1 class="pl-5 pt-5">Order food delivery you’ll love</h1>
                    <h3 class="pl-5 pt-5">Original website : <a href="https://www.grubhub.com" target="_blank">Grubhub</a></h3>
                    <h3 class="pl-5 pt-1">Project done by : <a href="https://gitlab.com/jayasurya17/273-lab-3" target="_blank">Jayasurya Pinaki</a></h3>
                    <h3 class="pl-5 pt-1">Student ID : 014491854</h3>
                </div>
            </div>
        )
    }
}
//export Home Component
export default Home